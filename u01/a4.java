import java.util.Random;

public class a4 {
	static long vergleiche;
	
	public static void main(String[] args) {
		float[] zahlen = new float[20_000_000];
		Random random = new Random();
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = random.nextFloat();
		}
		
		for (int b = 1; b < 100; b*=2) {
			long[] time = new long[10];
			long[] vergleicheArray = new long[10];
			for (int i = 0; i < 10; i++) {
				vergleiche = 0;
				long startTime = System.currentTimeMillis();
				mergesort(zahlen.clone(), b);
				long endTime = System.currentTimeMillis();
				time[i] = endTime - startTime;
				vergleicheArray[i] = vergleiche;
			}
			long timeAverage = 0;
			for (int i = 0; i < time.length; i++) {
				timeAverage += time[i];
			}
			timeAverage /= time.length;
			
			long vergleicheAverage = 0;
			for (int i = 0; i < vergleicheArray.length; i++) {
				vergleicheAverage += vergleicheArray[i];
			}
			vergleicheAverage /= vergleicheArray.length;
			
			System.out.println("b: " + b + "\tTime in ms: " + longArrayToString(time) + "\tDurchschnitt: " + timeAverage + "\tAnzahl der Vergleiche: " + longArrayToString(vergleicheArray) + "\tDurchschnitt: " + vergleicheAverage);
		}
	}
	
	public static float[] mergesort(float[] zahlen, int b) {
		if (zahlen.length < b) {
			return bubblesort(zahlen);
		} else if (zahlen.length < 2) {
			return zahlen;
		} else {
			int splitIndex = zahlen.length/2;
			float[] teilZahlen1 = new float[splitIndex];
			float[] teilZahlen2 = new float[zahlen.length - splitIndex];
			
			for (int i = 0; i < splitIndex; i++) {
				teilZahlen1[i] = zahlen[i];
			}
			
			for (int i = splitIndex; i < zahlen.length; i++) {
				teilZahlen2[i-splitIndex] = zahlen[i];
			}
			
			teilZahlen1 = mergesort(teilZahlen1, b);
			teilZahlen2 = mergesort(teilZahlen2, b);
			
			int pointer1 = 0;
			int pointer2 = 0;
			
			for (int i = 0; i < zahlen.length; i++) {
				if (pointer1 >= teilZahlen1.length) {
					zahlen[i] = teilZahlen2[pointer2];
					pointer2++;
				} else if (pointer2 >= teilZahlen2.length) {
					zahlen[i] = teilZahlen1[pointer1];
					pointer1++;
				} else if (teilZahlen1[pointer1]<=teilZahlen2[pointer2]) {
					vergleiche++;
					zahlen[i] = teilZahlen1[pointer1];
					pointer1++;
				} else {
					vergleiche++;
					zahlen[i] = teilZahlen2[pointer2];
					pointer2++;
				}
			}
			return zahlen;
		}
	}
	
	public static float[] bubblesort(float[] array) {
		boolean swapped;
		do {
			swapped = false;
			for (int i = 0; i < array.length-1; i++) {
				vergleiche++;
				if (array[i]>array[i+1]) {
					float tmp = array[i];
					array[i] = array[i+1];
					array[i+1] = tmp;
					swapped = true;
				}
			}
		} while (swapped);
		return array;
	}
	
	public static String floatArrayToString(float[] array) {
		String returnValue = "";
		for (int i = 0; i < array.length-1; i++) {
			returnValue += array[i] + ",";
		}
		returnValue += array[array.length-1];
		return returnValue;
	}
	
	public static String longArrayToString(long[] array) {
		String returnValue = "";
		for (int i = 0; i < array.length-1; i++) {
			returnValue += array[i] + ", ";
		}
		returnValue += array[array.length-1];
		return returnValue;
	}
}
